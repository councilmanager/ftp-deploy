import os
import uuid
import shutil

import boto3
import requests

from pathlib import Path

from bitbucket_pipes_toolkit.test import PipeTestCase
from bitbucket_pipes_toolkit.helpers import get_variable


class TargetHostPublicIpNotFound(Exception):
    pass


PIPE_ID_NAME = 'ftp'

AWS_USER = "ec2-user"
AWS_DEFAULT_REGION = "us-east-1"
AWS_STACK_NAME = f"bbci-pipes-test-infrastructure-ec2-{PIPE_ID_NAME}-{get_variable('BITBUCKET_BUILD_NUMBER')}"
REMOTE_PATH = f"/usr/share/nginx/html/{PIPE_ID_NAME}"
LOCAL_PATH = "test/tmp"


def get_target_host_ip(stack_name):
    client = boto3.client('ec2', region_name=AWS_DEFAULT_REGION)
    instances = client.describe_instances(
        Filters=[
            {'Name': 'tag:aws:cloudformation:stack-name', 'Values': [stack_name]},
            {'Name': 'instance-state-name', 'Values': ['running']},
        ]
    )
    try:
        return instances['Reservations'][0]['Instances'][0]['PublicIpAddress']
    except IndexError:
        raise TargetHostPublicIpNotFound(stack_name)


class FTPDeployTestCase(PipeTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        # get Variables
        cls.USER = AWS_USER
        cls.SERVER_IP = get_target_host_ip(AWS_STACK_NAME)
        cls.PASSWORD = get_variable('PASSWORD')
        cls.LOCAL_PATH = LOCAL_PATH
        cls.REMOTE_PATH = REMOTE_PATH
        cls.BASE_URL = f"http://{cls.SERVER_IP}/{PIPE_ID_NAME}"

    def setUp(self):
        self.api_client = self.docker_client.api
        self.filename = str(uuid.uuid4())
        os.mkdir(self.LOCAL_PATH)
        with open(os.path.join(os.getcwd(), self.LOCAL_PATH, self.filename), 'a'):
            pass

    def tearDown(self):
        shutil.rmtree(self.LOCAL_PATH, ignore_errors=True)

    def test_fail_no_parameters(self):
        result = self.run_container()
        self.assertIn('USER variable missing', result)

    def test_ftp_deployment_successful(self):
        result = self.run_container(environment={
            'USER': self.USER,
            'PASSWORD': self.PASSWORD,
            'SERVER': self.SERVER_IP,
            'REMOTE_PATH': self.REMOTE_PATH,
            'BITBUCKET_CLONE_DIR': os.path.join(os.getcwd(), 'test')
        })

        self.assertRegexpMatches(result, r'✔ Deployment finished.')

    def test_file_are_present_after_deploy(self):
        result = self.run_container(environment={
            'USER': self.USER,
            'PASSWORD': self.PASSWORD,
            'SERVER': self.SERVER_IP,
            'REMOTE_PATH': self.REMOTE_PATH,
            'LOCAL_PATH': self.LOCAL_PATH,
        }, stderr=True)

        response = requests.get(f'{self.BASE_URL}/{self.filename}')

        self.assertRegexpMatches(result, r'✔ Deployment finished.')
        self.assertEqual(response.status_code, 200)

    def test_ftp_deployment_exclude_file(self):
        filename_to_exclude = "filename_to_exclude.txt"
        Path(Path.cwd() / self.LOCAL_PATH / filename_to_exclude).touch()

        extra_args = "--exclude=filename_to_exclude.txt"

        result = self.run_container(environment={
            'USER': self.USER,
            'PASSWORD': self.PASSWORD,
            'SERVER': self.SERVER_IP,
            'REMOTE_PATH': self.REMOTE_PATH,
            'LOCAL_PATH': self.LOCAL_PATH,
            'EXTRA_ARGS': extra_args
        }, stderr=True)

        response = requests.get(f'{self.BASE_URL}/{filename_to_exclude}')

        self.assertRegexpMatches(result, r'✔ Deployment finished.')
        self.assertEqual(response.status_code, 404)

    def test_file_present_if_delete_flag_is_false(self):
        # create empty test file
        test_file = "test_file.txt"
        Path(Path.cwd() / self.LOCAL_PATH / test_file).touch()

        result = self.run_container(environment={
            'USER': self.USER,
            'PASSWORD': self.PASSWORD,
            'SERVER': self.SERVER_IP,
            'REMOTE_PATH': self.REMOTE_PATH,
            'LOCAL_PATH': self.LOCAL_PATH,
        }, stderr=True)

        self.assertRegexpMatches(result, r'✔ Deployment finished.')

        # remove test file from local directory
        Path(Path.cwd() / self.LOCAL_PATH / test_file).unlink()

        # run with DELETE_FLAG = false
        result = self.run_container(environment={
            'USER': self.USER,
            'PASSWORD': self.PASSWORD,
            'SERVER': self.SERVER_IP,
            'REMOTE_PATH': self.REMOTE_PATH,
            'LOCAL_PATH': self.LOCAL_PATH,
            'DELETE_FLAG': 'false'
        }, stderr=True)

        self.assertRegexpMatches(result, r'✔ Deployment finished.')

        # check if test file present
        response = requests.get(f'{self.BASE_URL}/{test_file}')
        self.assertEqual(response.status_code, 200)
