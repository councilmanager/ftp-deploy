# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.2

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.3.0

- minor: Added DELETE_FLAG variable: Made --delete-first optional.

## 0.2.3

- patch: Add possibility to use extra arguments

## 0.2.2

- patch: Internal maintenance: Add auto infrastructure for tests.

## 0.2.1

- patch: Documentaion update

## 0.2.0

- minor: Disabled ssl when initiating the connection which caused some issues when running the pipe

## 0.1.1

- patch: Update pipes bash toolkit version.

## 0.1.0

- minor: Initial release

