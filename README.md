# Bitbucket Pipelines Pipe: Deploy your code using FTP

Deploys your code to a remote server via the FTP protocol.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: atlassian/ftp-deploy:0.3.2
    variables:
      USER: '<string>'
      PASSWORD: '<string>'
      SERVER: '<string>'
      REMOTE_PATH: '<string>'
      # LOCAL_PATH: '<string>' # Optional
      # DEBUG: '<boolean>' # Optional
      # EXTRA_ARGS: '<string>' # Optional.
      # DELETE_FLAG: '<boolean>' # Optional.
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| USER (*)              | The name of the FTP user for your connection. |
| PASSWORD (*)          | The password for your FTP user. |
| SERVER (*)            | FTP server address. |
| REMOTE_PATH (*)       | The remote directory to upload the files. |
| LOCAL_PATH            | Optional path to local directory to upload. Default `${BITBUCKET_CLONE_DIR}`. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |
| EXTRA_ARGS            | Additional arguments passed to the lftp command (see [LFTP docs](http://manpages.ubuntu.com/manpages/trusty/en/man1/lftp.1.html) for more details). |
| DELETE_FLAG           | Use `--delete-first` option to delete old files before transferring new ones. Default: `true`. |

_(*) = required variable._

## Prerequisites

Since FTP uses two channels for command and data transfer and we use the passive FTP mode to transfer files to avoid issues when using NATs and firewalls. You should pay 
attention to this when configuring your FTP server.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/ftp-deploy:0.3.2
    variables:
      USER: my-ftp-user
      PASSWORD: $FTP_PASSWORD
      SERVER: 127.0.0.1
      REMOTE_PATH: /tmp/my-remote-directory
```

Example how to exclude files.

```yaml
script:
  - pipe: atlassian/ftp-deploy:0.3.2
    variables:
      USER: my-ftp-user
      PASSWORD: $FTP_PASSWORD
      SERVER: 127.0.0.1
      REMOTE_PATH: /tmp/my-remote-directory
      LOCAL_PATH: /my-working-directory,
      EXTRA_ARGS: "--exclude=filename_to_exclude.txt --exclude=folder_to_exclude/"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,ftp